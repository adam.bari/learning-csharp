﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;


namespace learning_csharp
{
    public class GitlabRestApi : IGitlabApi
    {
        private readonly ILogger<GitlabRestApi> _logger;
        private readonly GitlabOptions _options;

        public GitlabRestApi(ILogger<GitlabRestApi> logger, IOptions<GitlabOptions> options)
        {
            _logger = logger;
            _options = options.Value;
        }

        public async Task<List<dynamic>> FetchProjectMilestones(IEnumerable<string> projectIdList)
        {
            var projectMilestones = new List<dynamic>();
            await projectIdList.ToObservable()
                .Select(projectId => Observable.FromAsync(() => FetchProjectMilestones(projectId)))
                .Merge(5)
                .Do(t => projectMilestones.AddRange(t));
            return projectMilestones;
        }
    

        public async Task<List<dynamic>> FetchProjectMilestones(string projectId)
        {
            var url = new System.Text.StringBuilder("");
            url.Append($"https://gitlab.com/api/v4/projects/{projectId}");
            url.Append($"/milestones/?per_page=100&private_token={_options.PrivateToken}");
            var nextPage = "1";
            var milestones = new List<dynamic>();
            while (!String.IsNullOrEmpty(nextPage))
            {
                _logger.LogDebug($"getting milestones for project {projectId} - page {nextPage}");
                var milestoneRequest = await url
                    .SetQueryParams(new { page = nextPage })
                    .GetAsync();
                nextPage = milestoneRequest.Headers["X-Next-Page"];
                var milestoneList = await milestoneRequest.GetJsonListAsync();
                milestones.AddRange(milestoneList);
            }
            return milestones;
        }

        public async Task<List<dynamic>> FetchIssues()
        {
            var url = new System.Text.StringBuilder("");
            url.Append($"https://gitlab.com/api/v4/groups/{_options.GroupId}");
            url.Append($"/issues/?state=all&per_page=100&private_token={_options.PrivateToken}");
            var nextPage = "1";
            var issues = new List<dynamic>();
            while (!String.IsNullOrEmpty(nextPage))
            {
                _logger.LogDebug($"getting page {nextPage}");
                var issuesRequest = await url
                    .SetQueryParams(new { page = nextPage })
                    .GetAsync();
                nextPage = issuesRequest.Headers["X-Next-Page"];
                var issueList = await issuesRequest.GetJsonListAsync();
                issues.AddRange(issueList);
            }
            return issues;
        }
    }
}
