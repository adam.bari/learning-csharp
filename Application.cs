using System.Linq;
using System.Reflection;
using Autofac;

namespace learning_csharp
{
    public class Application: IApplication
    {
        public IContainer Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>().As<IApplication>();
            builder.RegisterAssemblyTypes(Assembly.Load("learning_csharp.Utilities"))
                .As(t => t.GetInterfaces().FirstOrDefault(i => i.Name == "I" + t.Name));
            return builder.Build();
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}