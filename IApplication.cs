namespace learning_csharp
{
    public interface IApplication
    {
        void Run();
    }
}