﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace learning_csharp
{
    public interface IGitlabApi
    {
        Task<List<dynamic>> FetchProjectMilestones(IEnumerable<string> projectIdList);

        Task<List<dynamic>> FetchProjectMilestones(string projectId);

        Task<List<dynamic>> FetchIssues();
    }
}
