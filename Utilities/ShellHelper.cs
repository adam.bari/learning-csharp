using System;
using System.Diagnostics;

namespace learning_csharp.Utilities
{
    public class ShellHelper : IShellHelper
    {
        public string Bash(string cmd)
        {
            Console.WriteLine("Let's execute: " + cmd);
            var escapedArgs = cmd.Replace("\"", "\\\"");
            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            Console.Write(result);
            process.WaitForExit();
            return result;
        }
    }
}
