using Google.Cloud.BigQuery.V2;

namespace learning_csharp.Utilities
{
    public interface IBigQuery
    {
        public BigQueryClient client{ get; }
    }
}