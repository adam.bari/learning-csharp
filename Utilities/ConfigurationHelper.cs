using System;
using System.IO;
using System.Text.Json;

namespace learning_csharp.Utilities
{
    public class Config : IConfig
    {
        public string GCloudCredentialsFile;
        public void LoadConfigFile()
        {
            try
            {
                using (StreamReader sr = new StreamReader("appsettings.json"))
                {
                    String content = sr.ReadToEnd();
                    var config = JsonSerializer.Deserialize<IConfig>(content);
                    GCloudCredentialsFile = config.gcloudcredfile;
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }
    }
}