using System;
using Google.Apis.Auth.OAuth2;

namespace learning_csharp.Utilities
{
    public class GCloud : IGCLoud
    {
        public GoogleCredential credential{
            get {
                Config _config = new Config();
                _config.LoadConfigFile();
                Console.WriteLine(_config.GCloudCredentialsFile);
                return GoogleCredential.FromFile(_config.GCloudCredentialsFile);
            }
        }
    }
}
