using System;
using Google.Cloud.BigQuery.V2;

namespace learning_csharp.Utilities
{
    public class BigQuery : IBigQuery
    {
        public BigQueryClient client { get; private set; }

        public BigQuery()
        {
            GCloud gc = new GCloud();
            client = BigQueryClient.Create("banco-barigui", gc.credential);
            string query = @"SELECT
                CONCAT(
                    'https://stackoverflow.com/questions/',
                    CAST(id as STRING)) as url, view_count
                FROM `bigquery-public-data.stackoverflow.posts_questions`
                WHERE tags like '%google-bigquery%'
                ORDER BY view_count DESC
                LIMIT 10";
            var result = client.ExecuteQuery(query, parameters: null);
            foreach (var row in result)
            {
                Console.WriteLine($"{row["url"]}: {row["view_count"]} views");
            }
        }
    }
}