using System;
using System.IO;
using System.Text;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;

namespace learning_csharp
{
    public class Bucket : IBucket
    {
        StorageClient client;
        public Bucket()
        {
            GCloud gc = new GCloud();
            client = StorageClient.Create(gc.credential);
            var buckets = client.ListBuckets("banco-barigui");
            foreach (var bucket in buckets)
            {
                Console.WriteLine(bucket.Name);
            }
            PutFiles("bari-test-adam");
        }

        public void PutFiles(string bucketName)
        {
            var content = Encoding.UTF8.GetBytes("hello, world");
            var obj1 = client.UploadObject(
                bucketName,
                "file1.txt",
                "text/plain",
                new MemoryStream(content)
            );
            var obj2 = client.UploadObject(
                bucketName,
                "folder1/file2.txt",
                "text/plain",
                new MemoryStream(content)
            );
        }
    }
}
