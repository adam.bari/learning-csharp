﻿using System;
using System.Collections.Generic;
using System.Text;

namespace learning_csharp
{
    public interface IGitlabOptions
    {
        public string PrivateToken { get; set; }

        public string GroupId { get; set; }
    }
}
